
const { DATABASE_NAME, TABLE_COMMENT } = require('../const.js');


class Comment {
    id;
    postId;
    username;
    comment;

    constructor(postId, username, comment) {
        this.postId = postId;
        this.username = username;
        this.comment = comment;
    }

    addComment(mysqlConnection) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                }
                
                const sql = `INSERT INTO ${TABLE_COMMENT} VALUES (NULL, '${this.postId}', '${this.username}', '${this.comment}');`;
                console.log(sql);
                mysqlConnection.query(sql, function (err, result) {
                    console.log(result);
                    if (err) {
                        reject(err);
                    }
                    resolve(result);
                    return;
                });
            });
        });
    }

    static getComments(mysqlConnection) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                }
                
                const sql = `SELECT * FROM ${TABLE_COMMENT};`;
                console.log(sql);
                mysqlConnection.query(sql, function (err, result) {
                    console.log(result);
                    if (err) {
                        reject(err);
                    }
                    resolve(result);
                    return;
                });
            });
        });
    }

    static getCommentsWithId(mysqlConnection, postId) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                }
                
                //console.log('getCommentsWithId', getCommentsWithId);
                const sql = `SELECT * FROM ${TABLE_COMMENT} WHERE postId = '${postId}';`;
                console.log(sql);
                mysqlConnection.query(sql, function (err, result) {
                    console.log(result);
                    if (err) {
                        reject(err);
                    }
                    resolve(result);
                    return;
                });
            });
        });
    }
    

    initializeCommentTable(mysqlConnection) {
        mysqlConnection.query(`
            CREATE TABLE IF NOT EXISTS ${TABLE_COMMENT} (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                postId SMALLINT UNSIGNED NOT NULL,
                username VARCHAR(40) NOT NULL,
                comment TEXT NOT NULL,
                PRIMARY KEY (id)
            )
            ENGINE=INNODB;`,
            (err, result) => {
            if (err) {
                throw err;
            }
        });
    }
};

module.exports = Comment;