
const { DATABASE_NAME, TABLE_USER } = require('../const.js');
const bcrypt = require('bcrypt');


class User {
    pseudo;
    password;
    _id;

    constructor(pseudo, password) {
        this.pseudo = pseudo;
        this.password = password;
    }

    addUser(mysqlConnection) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                }
                
                let pseudo = this.pseudo;
                let password = this.password;
                
                const sql = `SELECT pseudo FROM ${TABLE_USER} WHERE pseudo = '${pseudo}';`;
                mysqlConnection.query(sql, function (err, result) {
                    console.log(err, result);
                    if (err) {
                        reject(err);
                    } else if (result.length === 0) {
                        
                        const sql = `INSERT INTO ${TABLE_USER} VALUES (NULL, '${pseudo}', '${password}', NULL);`;
                        console.log(sql);
                        mysqlConnection.query(sql, function (err, result) {
                            console.log(result);
                            if (err) {
                                reject(err);
                            }
                            resolve(result.affectedRows);
                            return;
                        });
                    } else {
                        resolve('Pseudo already used.');
                        return;
                    }
                });
            });
        });
    }

    logUser(mysqlConnection, pseudo, password) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                
                mysqlConnection.query(`SELECT password FROM ${TABLE_USER} WHERE pseudo = '${pseudo}';`, function (err, result) {
                    if (err) {
                        reject(err);
                        return;
                    }
                    if (JSON.parse(JSON.stringify(result)).length === 0) {
                        resolve('Problème d\'authentification');
                        return;
                    }
                    result = Object.values(JSON.parse(JSON.stringify(result)))[0].password;
                    
                    bcrypt.compare(password, result)
                    .then(valid => {
                        if (!valid) {
                            resolve('Problème d\'authentification');
                            return;
                        } else {
                            resolve(true);
                            return;
                        }
                    })
                    .catch(err => {
                        reject('Impossible to decrypt the password..');
                        return;
                    })
                });
            });
        });
    }

    static getId(mysqlConnection, pseudo) {
        const alias = `Locked${TABLE_USER}`;
        return User.sqlQuery(mysqlConnection, `SELECT id FROM ${TABLE_USER} AS ${alias} WHERE pseudo = '${pseudo}';`, alias)
    }

    static isAdmin(mysqlConnection, pseudo) {
        const alias = `Locked${TABLE_USER}`;
        return User.sqlQuery(mysqlConnection, `SELECT admin FROM ${TABLE_USER} AS ${alias} WHERE pseudo = '${pseudo}';`, alias)
    }

    static getUsernameFromId(mysqlConnection, id) {
        const alias = `Locked${TABLE_USER}`;
        return User.sqlQuery(mysqlConnection, `SELECT pseudo FROM ${TABLE_USER} AS ${alias} WHERE id = '${id}';`, alias)
    }

    delete(mysqlConnection) {
        const alias = `Locked${TABLE_USER}`;
        return User.sqlQuery(mysqlConnection, `DELETE ${alias} FROM ${TABLE_USER} AS ${alias} WHERE ${alias}.pseudo = '${this.pseudo}';`, alias)
    }

    static sqlQuery(mysqlConnection, sql, alias) {
        return new Promise((resolve, reject) => {
            console.log(`USE ${DATABASE_NAME};`);
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                }

                console.log(`LOCK TABLES ${TABLE_USER} AS ${alias} WRITE;`);
                mysqlConnection.query(`LOCK TABLES ${TABLE_USER} AS ${alias} WRITE;`, (err, result) => {
                    
                    if (err) {
                        reject(err);
                    }
                    console.log(sql);
                    mysqlConnection.query(sql, function (err, result) {
                        console.log(err, result);
                        if (err) {
                            User.unlockTables(mysqlConnection);
                            reject(err);
                        } else if (result) {
                            User.unlockTables(mysqlConnection);
                            resolve(result);
                        } else {
                            User.unlockTables(mysqlConnection);
                            reject(`Problem encountered while trying to query : ${sql}`);
                        }
                    });
                });
            });
        });
    }

    static unlockTables(mysqlConnection) {
        console.log(`UNLOCK TABLES;`);
        mysqlConnection.query(`UNLOCK TABLES;`, (err, result) => { });
    }

    initializeUserTable(mysqlConnection) {
        // à placer dans un fichier sql..
        mysqlConnection.query(`
            CREATE TABLE IF NOT EXISTS ${TABLE_USER} (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                pseudo VARCHAR(40) NOT NULL,
                password VARCHAR(60) NOT NULL,
                PRIMARY KEY (id)
            )
            ENGINE=INNODB;`,
            (err, result) => {
            if (err) {
                throw err;
            }
        });
    }
};

module.exports = User;
