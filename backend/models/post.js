const { DATABASE_NAME, TABLE_POST, TABLE_USER } = require('../const.js');

module.exports = class Post {
    text;
    imageUrl;
    userId;
    

    constructor(text, imageUrl, userId) {
        this.text = text;
        this.imageUrl = imageUrl;
        this.userId = userId;
    }

    create(mysqlConnection) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                mysqlConnection.query(`INSERT INTO ${TABLE_POST} VALUES (NULL, '${this.text}', '${this.imageUrl}', '${this.userId}');`, function (err, result) {
                    if (err) {
                        reject(err);
                        console.log(err);
                        return;
                    }
                    console.log(result);
                    resolve(result);
                    return;
                });
            });
        });
    }

    static findAll(mysqlConnection) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }

                const sql =     `SELECT ${TABLE_POST}.id, ${TABLE_POST}.text, ${TABLE_POST}.imageUrl, ${TABLE_USER}.pseudo FROM ${TABLE_POST}
                                INNER JOIN ${TABLE_USER}
                                ON ${TABLE_POST}.userId = ${TABLE_USER}.id;`
                console.log(sql);
                mysqlConnection.query(sql, function (err, result) {
                    if (err) {
                        reject(err);
                        console.log(err);
                        return;
                    }
                    //console.log(result);
                    resolve(result);
                    return;
                });
            });
        });
    }

    static delete(mysqlConnection, _id) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                const sql = `DELETE FROM ${TABLE_POST} WHERE id = ${_id};`;
                mysqlConnection.query(sql, function (err, result) {
                    if (err) {
                        reject(err);
                        console.log(err);
                        return;
                    }
                    console.log(result);
                    resolve(result);
                    return;
                });
            });
        });
    }

    static update(mysqlConnection, _id, text, imageUrl) {
        return new Promise((resolve, reject) => {
            mysqlConnection.query(`USE ${DATABASE_NAME};`, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                const sql = `UPDATE ${TABLE_POST} SET text = '${text}', imageUrl = '${imageUrl}' WHERE id = ${_id}`;
                console.log(sql);
                mysqlConnection.query(sql, function (err, result) {
                    if (err) {
                        reject(err);
                        console.log(err);
                        return;
                    }
                    console.log(result);
                    resolve(result);
                    return;
                });
            });
        });
    }

    initializePostTable(mysqlConnection) {
        // à placer dans un fichier sql..
        mysqlConnection.query(`
            CREATE TABLE IF NOT EXISTS ${TABLE_POST} (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                text TEXT NOT NULL,
                imageUrl TEXT NOT NULL,
                userId SMALLINT UNSIGNED NOT NULL,
                PRIMARY KEY (id)
            )
            ENGINE=INNODB;`,
            (err, result) => {
            if (err) {
                throw err;
            }
        });
    }
}
