const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mysql = require('mysql');
const User = require('../models/user');

const { USER_DATABASE, HOST_DATABASE, PASSWORD_DATABASE } = require('../const.js');

exports.subscribe = (req, res, next) => {

    /*if (req.body.password && req.body.pseudo && req.body.password != req.body.pseudo) {
      if (/[a-z]/.test(req.body.password) && /[A-Z]/.test(req.body.password) && /[0-9]/.test(req.body.password) && /[^\w]/.test(req.body.password) && req.body.password.length > 7) {
        
      } else {
        res.status(400).json({message: "Le mot de passe ne respecte pas le format préconisé !"});
        return;
      }
    } else {
      res.status(400).json({message: "Le nom doit être différent du mot de passe !"});
      return;
    }*/


    bcrypt.hash(req.body.password, 12)
    .then(hash => {
        let user = new User(req.body.pseudo, hash);
        const db = mysqlConnection();

        db.connect(function(err) {
          if (err) throw err;
          
          
          user.addUser(db)
          .then(result => {
            console.log('result', result);
            if (result === 1) {
              res.status(200).json({message: "Welcome to Groupomania !"});
              return;
            } else if (result === 'Pseudo already used.') {
              res.status(200).json({message: result});
            } else {
              res.status(400).json({message: "Compte non créé !"});
              return;
            }
          })
          .catch(error => {
            console.log(error);
            res.status(400).json({error});
            return;
          });
        });
    })
};

exports.login = (req, res, next) => {

  let user = new User('', '');
  const db = mysqlConnection();

  db.connect(function(err) {
    if (err) throw err;
    
    user.logUser(db, req.body.pseudo, req.body.password)
    .then(result => {
      if (result === true) {
        User.getId(db, req.body.pseudo)
        .then(id => {

          res.status(200).json({
            userId: id[0].id,
            token: jwt.sign(
                { userId: id[0].id },
                'fesf758es75f4es32f4esf6874es6fes',
                { expiresIn: '12h' }
            )
          });
          return;
        })
        .catch(error => {
          console.log(404);
          res.status(404).json({ error: error });
          return;
        });
      } else if (result === 'Problème d\'authentification') {
          res.status(200).json({ result });
          return;
      } else {
        throw new Error("Wrong pseudo/password..");
      }
    })
    .catch(error => {
      console.log(error);
      res.status(501).json({ error });
      return;
    });
  })
};

exports.delete = (req, res, next) => {

  let user = new User(req.body.pseudo, '');
  const db = mysqlConnection();

  db.connect(function(err) {
    
    if (err) throw err;
    user.delete(db)
    .then(result => {
      console.log('result', result);
      if (result) {
        res.status(200).json({ message: 'Account deleted !' });
        return;
      } else {
        throw new Error("Wrong pseudo/password..");
      }
    })
    .catch(error => {
      res.status(500).json({ error: error });
      return;
    });
  })
};

exports.getUserFromId = (req, res, next) => {
  const db = mysqlConnection();

  db.connect(function(err) {
    
    if (err) throw err;
    User.getUsernameFromId(db, req.body.id)
    .then(result => {
      res.status(200).json({ username: result });
      return;
    })
    .catch(error => {
      res.status(500).json({ error: error });
      return;
    });
  })
};

exports.isAdmin = (req, res, next) => {
  const db = mysqlConnection();

  db.connect(function(err) {
    
    if (err) throw err;
    User.isAdmin(db, req.body.username)
    .then(result => {
      res.status(200).json({ username: result });
      return;
    })
    .catch(error => {
      res.status(500).json({ error: error });
      return;
    });
  })

  
};


mysqlConnection = () => {
  return mysql.createConnection({
    host: HOST_DATABASE,
    user: USER_DATABASE,
    password: PASSWORD_DATABASE,
  });
}
