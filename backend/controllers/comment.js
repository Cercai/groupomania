const mysql = require('mysql');
const Comment = require('../models/comment');

const { USER_DATABASE, HOST_DATABASE, PASSWORD_DATABASE } = require('../const.js');

exports.createComment = (req, res, next) => {
    const db = mysql.createConnection({
        host: HOST_DATABASE,
        user: USER_DATABASE,
        password: PASSWORD_DATABASE,
    });

    db.connect(function(err) {
        if (err) throw err;
       
        const comment = new Comment(req.body.postId, req.body.user, req.body.comment);
        comment.addComment(db)
        .then(() => res.status(201).json({ message: 'Comment created !' }))
        .catch(error => res.status(400).json({ error: error }));
    });
};

exports.getAllComments = (req, res, next) => {
    const db = mysql.createConnection({
        host: HOST_DATABASE,
        user: USER_DATABASE,
        password: PASSWORD_DATABASE,
    });

    db.connect(function(err) {
        if (err) throw err;
       
        Comment.getComments(db)
        .then((result) => res.status(201).json(result))
        .catch(error => res.status(400).json({ error: error }));
    });
};

exports.getCommentsWithId = (req, res, next) => {
    const db = mysql.createConnection({
        host: HOST_DATABASE,
        user: USER_DATABASE,
        password: PASSWORD_DATABASE,
    });

    db.connect(function(err) {
        if (err) throw err;
       
        Comment.getCommentsWithId(db, req.params.id)
        .then((result) => res.status(201).json(result))
        .catch(error => res.status(400).json({ error: error }));
    });
};
