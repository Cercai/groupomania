const fs = require('fs');
const mysql = require('mysql');
const Post = require('../models/post');

const { USER_DATABASE, HOST_DATABASE, PASSWORD_DATABASE } = require('../const.js');


exports.findAll = (req, res, next) => {
    const db = mysql.createConnection({
        host: HOST_DATABASE,
        user: USER_DATABASE,
        password: PASSWORD_DATABASE,
    });

    db.connect(function(err) {
        if (err) throw err;
        Post.findAll(db)
        .then(posts => res.status(200).json(posts))
        .catch(error => res.status(401).json({ error }));
    });
};

exports.createPost = (req, res, next) => {
    const db = mysql.createConnection({
        host: HOST_DATABASE,
        user: USER_DATABASE,
        password: PASSWORD_DATABASE,
    });

    db.connect(function(err) {
        if (err) throw err;
        console.log(req.body.post);
        const bodyPost = JSON.parse(req.body.post);
       
        const post = new Post(bodyPost.text, `${req.protocol}://${req.get('host')}/images/${req.file.filename}`, bodyPost.userId);
        post.create(db)
        .then(() => res.status(201).json({ message: 'Post created !' }))
        .catch(error => res.status(400).json({ error: error }));
    });
};

exports.deletePost = (req, res, next) => {
    const db = mysql.createConnection({
        host: HOST_DATABASE,
        user: USER_DATABASE,
        password: PASSWORD_DATABASE,
    });

    db.connect(function(err) {
        if (err) throw err;
        console.log(req.params.id);
        //const bodyPost = JSON.parse(req.body.post);
       Post.delete(db, req.params.id)
        .then(() => res.status(201).json({ message: 'Post deleted !' }))
        .catch(error => res.status(400).json({ error: error }));
    });
};

exports.deleteSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id })
      .then(sauce => {
        const filename = sauce.imageUrl.split('/images/')[1];
        fs.unlink(`images/${filename}`, () => {
            sauce.deleteOne({ _id: req.params.id })
            .then(() => res.status(200).json({ message: 'Sauce supprimée !'}))
            .catch(error => res.status(400).json({ error }));
        });
      })
      .catch(error => res.status(500).json({ error }));
};

exports.updatePost = (req, res, next) => {

    const postObject = req.file ?
    {
        ...JSON.parse(req.body.post),
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    } : { ...req.body };
    console.log(postObject);

    const db = mysql.createConnection({
        host: HOST_DATABASE,
        user: USER_DATABASE,
        password: PASSWORD_DATABASE,
    });
    console.log({ _id: req.params.id });
    Post.update(db, req.params.id, postObject.text, `${req.protocol}://${req.get('host')}/images/${req.file.filename}`)
    .then(() => res.status(200).json({ message: 'Post modifié !'}))
    .catch(error => res.status(400).json({ error }));
};