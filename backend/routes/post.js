const express = require('express');
const router = express.Router();

const postController = require('../controllers/post');
const auth = require('../middlewares/auth');
const multer = require('../middlewares/multer-config');

router.get('/wall', auth, postController.findAll);
router.post('/newpost', auth, multer, postController.createPost);
router.delete('/deletePost:id', auth, postController.deletePost);
/*router.get('/:id', auth, postController.findOne);*/
router.put('/editPost:id', auth, multer, postController.updatePost);

module.exports = router;