const express = require('express');
const router = express.Router();

const commentController = require('../controllers/comment');
const auth = require('../middlewares/auth');

router.post('/comment', auth, commentController.createComment);
router.get('/comments', auth, commentController.getAllComments);
router.get('/comments:id', auth, commentController.getCommentsWithId);

module.exports = router;