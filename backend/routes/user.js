const express = require('express');
const router = express.Router();

const userController = require('../controllers/user');

router.post('/subscribe', userController.subscribe);
router.post('/login', userController.login);
router.post('/delete', userController.delete);
router.post('/getUserFromId', userController.getUserFromId);
router.post('/isAdmin', userController.isAdmin);


module.exports = router;