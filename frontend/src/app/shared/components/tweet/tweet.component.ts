import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { faEdit, faBan, faThumbsUp, faComment } from '@fortawesome/free-solid-svg-icons';
import { PostService } from 'src/app/core/services/post.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Post } from 'src/app/core/models/post';
import { Comment } from 'src/app/core/models/comment';
import { CommentService } from 'src/app/core/services/comment.service';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.scss']
})
export class TweetComponent implements OnInit {
  
  @Input() public imageUrl = 'image';
  @Input() public text = 'text';
  @Input() public pseudo = 'pseudo';
  @Input() public ownPost = false;
  @Input() public admin = false;
  @Input() public _id = '';

  @Output() deletePostEvent = new EventEmitter<boolean>();
  @Output() editPostEvent = new EventEmitter<void>();
  @Output() newCommentEvent = new EventEmitter<void>();

  public username: string = '';
  public faEdit = faEdit;
  public faBan = faBan;
  public faThumbsUp = faThumbsUp;
  public postEditForm!: FormGroup;
  public commentForm!: FormGroup;
  public imagePreview = '';
  public isCollapsed = true;
  public faComment = faComment;
  public comments!: Comment[];


  constructor(private readonly userService: UserService,
    private readonly postService: PostService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private readonly commentService: CommentService,
    ) { }

  ngOnInit(): void {
    this.username = this.pseudo;
    this.initEditForm();
    this.commentService.getCommentsWithPostId(this._id)
    .subscribe(comments => {
      this.comments = [];
      comments.forEach((comment: { id: string; postId: string; username: string; comment: string; }) => {
        this.comments.push(new Comment(comment.id, comment.postId, comment.username, comment.comment));
      });
      console.log(this.comments);
    });
  }

  deleteTweet(): void {
    this.postService.deletePost(this._id)
    .subscribe(result => {
      if (result.error) {
        console.error(result.error);
        return;
      }
      this.deletePostEvent.emit(true);
    });
  }

  initEditForm() {
    this.postEditForm = this.formBuilder.group({
      editText: ['', Validators.required],
      image: ['', Validators.required],
    });

    this.commentForm = this.formBuilder.group({
      comment: ['', Validators.required],
    });
  }

  onSubmitForm() {
    const formValue = this.postEditForm.value;
    const editedPost = new Post(
      this.userService.getUserId(),
      formValue['editText']
    );

    const image = this.postEditForm.get('image');
    if (image) {
      this.postService.editPost(editedPost, image.value, this._id)
      .subscribe(result => {
        this.editPostEvent.emit();

      });
    }
  }

  onSubmitComment() {
    const formValue = this.commentForm.value;
    const newComment = new Comment('',  this._id, this.userService.getUsername(), formValue['comment']);

    const image = this.postEditForm.get('image');
    if (image) {
      this.commentService.createComment(newComment)
      .subscribe(result => {
        console.log(result);
        this.newCommentEvent.emit();
      });
    }
  }

  editPost(content: any): void {
    this.initEditForm();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then(
      (result) => {

      console.log(this._id);
    }, (reason) => {
      console.log(this._id);
    });
  }

  onFileAdded(event: any) {
    if (event.target && this.postEditForm.get('image')) {
      const file = event.target.files[0];
      const lolz = this.postEditForm.get('image') as FormArray;
      lolz.setValue(file);
      this.postEditForm.updateValueAndValidity();

      const reader = new FileReader();
      reader.onload = () => {
          this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
    
  }
}
