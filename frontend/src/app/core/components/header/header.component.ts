import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { UserService } from '../../services/user.service';
import { faCommentDots, faCog, faUserFriends, faUserLock, faUserAlt, faUserEdit, faUserPlus, faBell, faUserTimes, faPowerOff } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isMenuCollapsed = true;
  public isAuth = false;
  public faUserAlt = faUserAlt;
  public faUserPlus = faUserPlus;
  public faBell = faBell;
  public faUserTimes = faUserTimes;
  public faCog = faCog;
  public faPowerOff = faPowerOff;
  public faUserFriends = faUserFriends;

  constructor(private readonly router: Router, 
    private readonly authenticationService: AuthenticationService,
    private readonly http: HttpClient,
    private readonly userService: UserService) { }

  ngOnInit(): void {
    this.authenticationService.isAuth$.subscribe(isAuth => this.isAuth = isAuth);
  }

  signout() {
    this.isAuth = false;
    
    this.authenticationService.emitIsAuth(false);
  }

  tweet() {
    this.router.navigate(['newpost']);
  }
  
  delete() {
    if (confirm('Do you really want to delete your account ?')) {
      this.userService.deleteUser()
      .subscribe(response => {
        console.log(response);
        //this.router.navigate(['login']);
        this.authenticationService.emitIsAuth(false);
      });
    }
  }
}
