import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { EmptyPost, Post } from '../../models/post';
import { PostService } from '../../services/post.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.scss']
})
export class WallComponent implements OnInit {

  public posts!: Post[];
  public username = '';
  public isCollapsed = true;
  public imagePreview = '';
  public postForm!: FormGroup;
  public isAdmin = false;
  

  constructor(private readonly postService: PostService,
    private readonly userService: UserService,
    private formBuilder: FormBuilder,
    private readonly router: Router,
    private modalService: NgbModal,) { }

  ngOnInit(): void {
    this.postService.getPosts().subscribe(
      (posts) => {
        posts.reverse();
        this.posts = posts;
        this.username = this.userService.getUsername();
        this.userService.isAdmin(this.username)
        .subscribe(admin => {
          if (admin.username[0].admin === 't') {
            
            this.isAdmin = true;
          }
        });
      },
      (error) => {
        console.error(error);
      }
    );

    this.initForm();
  }

  initForm() {
    this.postForm = this.formBuilder.group({
      text: ['', Validators.required],
      image: ['', Validators.required],
    });
  }

  

  onSubmitForm() {
    const formValue = this.postForm.value;
    const newPost = new Post(this.userService.getUserId(),
      formValue['text']
    );

    const image = this.postForm.get('image');
    if (image) {
      this.postService.addPost(newPost, image.value)
      .subscribe(result => {
        this.isCollapsed = true;
        this.getPosts();
      });
    } else {
      this.postService.addPost(newPost)
      .subscribe(result => {
        this.isCollapsed = true;
        this.getPosts();
      });
    }
  }

  getPosts() {
    this.postService.getPosts().subscribe(
      (posts: Post[]) => {
        posts.reverse();
        this.posts = posts;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  onFileAdded(event: any) {
    if (event.target && this.postForm.get('image')) {
      const file = event.target.files[0];
      const lolz = this.postForm.get('image') as FormArray;
      lolz.setValue(file);
      this.postForm.updateValueAndValidity();
      console.log(this.postForm);
      const reader = new FileReader();
      reader.onload = () => {
          this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }
}
