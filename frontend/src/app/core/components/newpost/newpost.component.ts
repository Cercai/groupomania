import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Post } from '../../models/post'
import { PostService } from '../../services/post.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-newpost',
  templateUrl: './newpost.component.html',
  styleUrls: ['./newpost.component.scss']
})
export class NewpostComponent implements OnInit {

  private imageUrl!: File;
  
  public imagePreview = '';
  public postForm!: FormGroup;
  
  constructor(private formBuilder: FormBuilder,
    private readonly postService: PostService,
    private readonly userService: UserService) {
      
     }


  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.postForm = this.formBuilder.group({
      text: ['', Validators.required],
      image: ['', Validators.required],
    });
  }

  onSubmitForm() {
    const formValue = this.postForm.value;
    const newPost = new Post(
      this.userService.getUserId(),
      formValue['text']
    );

    const image = this.postForm.get('image');
    if (image) {
      console.log(image.value);
      console.log('this.imagePreview', image.value);
      this.postService.addPost(newPost, image.value)
      .subscribe(result => {
        console.log(result);
      });
    } else {
      /*this.postService.addPost(newPost)
      .subscribe(result => {
        console.log(result);
      });*/
    }
    
    //this.router.navigate(['wall']);
  }

  onFileAdded(event: any) {
    if (event.target && this.postForm.get('image')) {
      const file = event.target.files[0];
      const lolz = this.postForm.get('image') as FormArray;
      lolz.setValue(file);
      this.postForm.updateValueAndValidity();
      console.log(this.postForm);
      const reader = new FileReader();
      reader.onload = () => {
          this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
    
  }

  /*handleFileInput($event: any) {
    console.log($event.target.files);
    if ($event.target.files) {
      this.imageUrl = $event.target.files[0];
    }
    //let input: InputFiles = document.getElementById('postImg');

    console.log( document.getElementById('imageUrl'));
    //formValue['postImg']
  }*/

}
