import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './components/header/header.component';
import {RouterModule} from '@angular/router';
import { LoginModule } from '../login/login.module';
import { BodyComponent } from './components/body/body.component';
import { WallComponent } from './components/wall/wall.component';
import { SharedModule } from '../shared/shared.module';
import { NewpostComponent } from './components/newpost/newpost.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [
    HeaderComponent,
    BodyComponent,
    WallComponent,
    NewpostComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SharedModule,
    RouterModule.forRoot([]),
    FontAwesomeModule,
  ],
  exports: [
    HeaderComponent,
    BodyComponent,
    WallComponent,
    LoginModule,
    NewpostComponent,
  ],
  bootstrap: [
    HeaderComponent,
    BodyComponent,
    WallComponent,
    NewpostComponent,
  ]
})
export class CoreModule { }
