export interface IComment {
    id: string,
    postId: string,
    user: string,
    comment: string,
}