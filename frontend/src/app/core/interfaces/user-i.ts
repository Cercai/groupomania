
export interface IUser {
    _id: number,
    pseudo: string,
    password: string,
}