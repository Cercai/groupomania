import { IUser } from "../interfaces/user-i";


export class User implements IUser {

    constructor (   public pseudo: string,
                    public password: string,
                    public _id: number) {
        
    }
}