import { IPost } from "../interfaces/post-i";
import { UserService } from "../services/user.service";



export class Post implements IPost {

    public username = '';
    public pseudo = '';
    
    constructor (
        public userId: string,
        public text: string,
        public imageUrl = '',
        public id = '',
        
        ) {
    }
}

export class EmptyPost implements IPost {
    public username = '';
    public pseudo = '';
    
    constructor (
        public userId = '',
        public text = '',
        public imageUrl = '',
        public id = '',
        
        ) {
    }
}
