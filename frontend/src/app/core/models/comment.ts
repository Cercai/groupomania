import { IComment } from "../interfaces/comment-i";


export class Comment implements IComment {

    constructor (public id: string,
        public postId: string,
        public user: string,
        public comment: string,) {
    }
    
}