import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Comment } from '../models/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private readonly http: HttpClient) { }

  createComment(comment: Comment): Observable<any> {
      return this.http.post<any>(`http://localhost:3000/comment`, comment);
  }

  getAllComments(): Observable<any> {
    return this.http.get<any>(`http://localhost:3000/comments`);
  }

  getCommentsWithPostId(postId: string): Observable<any> {
    return this.http.get<any>(`http://localhost:3000/comments${postId}`);
  }
}
