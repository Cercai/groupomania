import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private username = '';
  public message = '';
  public isAuth$ = new Subject<boolean>();
  public message$ = new Subject<string>();

  constructor(private readonly http: HttpClient,
    private readonly router: Router,
   ) {

   }

  login(user: User): any {
    
    this.http.post<any>(`http://localhost:3000/login`, user)
    .subscribe(response => {
      console.log(response.result);
      console.log(response.error);
      if (response.error) {
        this.emitIsAuth(false);
        this.message$.next(response.error);
        return response.error;
      } else if (response.result === 'Problème d\'authentification') {
        this.emitIsAuth(false);
        console.log(response.result);
        this.message$.next('Problème d\'authentification');
        return;
      }

      localStorage.setItem('username', JSON.stringify(user.pseudo));
      localStorage.setItem('userId', JSON.stringify(response.userId));
      localStorage.setItem('token', JSON.stringify(response.token));
      this.emitIsAuth(true);
      //this.username = user.pseudo;
      this.router.navigate(['wall']);
    });
  }

  public subscriptionMessage(): Subject<string> {
    return this.message$;
  }

  public emitIsAuth(isAuth: boolean) {
      /*if (isAuth === true) {
        console.log('setItem: ' + this.username);
        localStorage.setItem('username', JSON.stringify(this.username));
      } else {
        localStorage.clear();
      }*/
      if (isAuth === false) {
        localStorage.clear();
        this.router.navigate(['login']);
      }
      this.isAuth$.next(isAuth);
  }


}
