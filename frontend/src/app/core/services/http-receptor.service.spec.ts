import { TestBed } from '@angular/core/testing';

import { HttpReceptorService } from './http-receptor.service';

describe('HttpReceptorService', () => {
  let service: HttpReceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpReceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
