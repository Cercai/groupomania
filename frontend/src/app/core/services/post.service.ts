import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Post } from '../models/post';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  //private posts!: Post[];
  public posts$ = new Subject<Post[] | Object>();
  private posts: Post[] = [];


  constructor(private readonly http: HttpClient,
    private readonly userService: UserService) {
    
   }

   addPost(post: Post, image?: File): Observable<any> {
    this.posts.push(post);
    this.emitPosts();
    const formData = new FormData();
    formData.append('post', JSON.stringify(post));
    if (image) {
      formData.append('image', image);
    } else {
      formData.append('image', '');
    }
    
    console.log('formData', formData);
    return this.http.post<any>(`http://localhost:3000/newpost`, formData);
  }

  emitPosts() {
    this.posts$.next(this.posts.slice());
  }

  getPosts(): Observable<any> {
    return this.http.get('http://localhost:3000/wall');
  }

  deletePost(_id: string): Observable<any> {
    return this.http.delete(`http://localhost:3000/deletePost${_id}`);
  }

  editPost(post: Post, image: File, _id: string): Observable<any> {
    const formData = new FormData();
    formData.append('post', JSON.stringify(post));
    formData.append('image', image);

    return this.http.put(`http://localhost:3000/editPost${_id}`, formData);
  }
}
