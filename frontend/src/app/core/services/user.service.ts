import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users: User[] = [];
  private userSubject = new Subject<User[]>();

  constructor(private readonly http: HttpClient) {

  }

  emitUsers() {
    this.userSubject.next(this.users.slice());
  }

  addUser(user: User): Observable<any> {
    this.users.push(user);
    this.emitUsers();
    return this.http.post<any>(`http://localhost:3000/subscribe`, user);
  }

  deleteUser() {
    // mettre à jour le users array
    // this.emitUsers();
    console.log(this.getUsername());

    return this.http.post<any>(`http://localhost:3000/delete`, { pseudo: this.getUsername()});
  }

  getUsername(): string {
    let username = localStorage.getItem('username');
    if (username) {
      return JSON.parse(username);
    }
    return '';
  }

  getUserId(): string {
    let userId = localStorage.getItem('userId');
    if (userId) {
      return JSON.parse(userId);
    }
    return '';
  }

  getUserFromId(id: string): Observable<any> {
    return this.http.post<any>(`http://localhost:3000/getUserFromId`, { id: id });
  }

  getToken(): string {
    const token = localStorage.getItem('token');
    if (token && token != 'undefined') {
      return JSON.parse(token);
    }
    return '';
  }

  isAdmin(username: string): Observable<any> {
    return this.http.post<any>(`http://localhost:3000/isAdmin`, { username: username });
  }

}
