import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { UserService } from 'src/app/core/services/user.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

  public message = '';

  constructor(private readonly userService: UserService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly authenticationService: AuthenticationService,
    private modalService: NgbModal,) {

    }

  public loginForm!: FormGroup;

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.loginForm = this.formBuilder.group({
      pseudo: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmitForm(content: any) {
    const formValue = this.loginForm.value;
    const newUser = new User(
      formValue['pseudo'],
      formValue['password'],
      0
    );
    this.userService.addUser(newUser).subscribe(response => {
      if (response.message === 'Pseudo already used.') {
        this.message = 'Pseudo already used.';
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then();
      } else if (response.message === 'Welcome to Groupomania !') {
        this.message = 'Welcome to Groupomania !';
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then();
        this.authenticationService.login(newUser);
        this.router.navigate(['wall']);
      }
      else if (response.error) {
        alert(response.error);
      }
    });
  }
}
