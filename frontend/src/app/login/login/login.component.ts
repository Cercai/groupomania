import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/core/models/user';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public newUser!: FormGroup;
  public message = '';
  public content: any;

  constructor(private readonly authenticationService: AuthenticationService,
              private readonly formBuilder: FormBuilder,
              private readonly router: Router,
              private readonly modalService: NgbModal,
              ) {
      this.authenticationService.subscriptionMessage().subscribe(message => {
        this.message = message;
        this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title'}).result.then();
      });
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.newUser = this.formBuilder.group({
      pseudo: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  onSubmitForm(content: any) {
    const formValue = this.newUser.value;
    const newUser = new User(
      formValue['pseudo'],
      formValue['password'],
      0
    );

    const message = this.authenticationService.login(newUser);
    this.content = content;

    this.router.navigate(['wall']);
  }

}


