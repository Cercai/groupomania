import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { LoginModule } from './login/login.module';
import { LoginComponent } from './login/login/login.component';
import { SubscribeComponent } from './login/subscribe/subscribe.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { WallComponent } from './core/components/wall/wall.component';
import { AuthGuardService } from './core/services/auth-guard.service';
import { NewpostComponent } from './core/components/newpost/newpost.component';
import { HttpReceptorService } from './core/services/http-receptor.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'subscribe', component: SubscribeComponent},
  { path: 'wall', canActivate: [AuthGuardService], component: WallComponent},
  //{ path: 'newpost', /*canActivate: [AuthGuardService],*/ component: NewpostComponent},
  { path: '', component: LoginComponent},
];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CoreModule,
    NgbModule,
    RouterModule.forRoot(appRoutes),
    FontAwesomeModule,
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: HttpReceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
